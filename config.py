from os import environ, urandom


class Config:
    """
    Flask configuration
    """

    # General



class DevelopmentConfig(Config):
    """
    Dev configs
    """

    SECRET_KEY = 'so-secret'
    TESTING = True
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///data/card-stock.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    CACHE_TYPE = "simple"
    CACHE_DEFAULT_TIMEOUT = 30
    environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'


class ProductionConfig(Config):
    """
    Prod configs
    """
    # DATABASE
    DEBUG = False
    SECRET_KEY = environ.get('SECRET_KEY') or urandom(24)
    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CACHE_TYPE = "simple"
    # CACHE_TYPE = "memcached"
    # CACHE_MEMCACHED_SERVERS = environ.get('MEMCACHED_SERVERS')
    CACHE_DEFAULT_TIMEOUT = 30


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
}