import logging

from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask
from config import app_config
from logging.config import dictConfig
from flask_login import (
    LoginManager,)
from os import environ
from dotenv import load_dotenv
from oauth import Auth

load_dotenv(verbose=True)

# Database
db = SQLAlchemy()
cache = Cache()

# Authentication
auth = Auth()
login_manager = LoginManager()

# Logging configuration
log_file = 'logs/card-stock.log'


def create_app(env):
    """
    Construct core application
    """
    from app import models
    app = Flask('card-stock', instance_relative_config=True)
    app.config.from_object(app_config[env])
    app.config.from_object('config.Config')

    Migrate(app, db, compare_type=True, compare_server_default=True)
    db.init_app(app)

    log_level = logging.DEBUG

    if env == 'production':
        log_level = logging.INFO

    set_logging(log_level)
    cache.init_app(app)
    cache.clear()

    with app.app_context():
        from .main import routes
        app.register_blueprint(routes.main_blueprint)

        login_manager.init_app(app)
        login_manager.login_view = "main_blueprint.login"
        login_manager.session_protection = "strong"

        db.create_all()

        return app


def set_logging(log_level):
    logging.basicConfig(filename=log_file, level=log_level)

    dictConfig({
        'version': 1,
        'formatters': {
            'default': {
                'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
            }
        },
        'handlers': {
            'wsgi': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://flask.logging.wsgi_errors_stream',
                'formatter': 'default'
            },
            'file': {
                'class': 'logging.FileHandler',
                'filename': log_file,
                'mode': 'w',
                'formatter': 'default'
            },
            'mail': {
                'level': 'ERROR',
                'class': 'logging.handlers.SMTPHandler',
                'mailhost': ('smtp.mailgun.org', 587),
                'fromaddr': 'card-stock@deephalf.ca',
                'toaddrs': ['incoming+shibatek-public-dev-card-stock-18076301-issue-@incoming.gitlab.com', ],
                'subject': 'Error log detected',
                'credentials': ('card-stock@mg.deephalf.ca', environ.get('SMTP_PASSWORD', None)),
            }
        },
        'root': {
            'level': log_level,
            'handlers': ['wsgi', 'file', 'mail']
        }
    })
