from . import logger
from ebaysdk.finding import Connection as Finding
from ebaysdk.exception import ConnectionError
from typing import List, Dict
from app import cache


class EbayClient:
    def __init__(self, app_id):
        self.app_id = app_id
        self.api = Finding(appid=self.app_id, siteid='EBAY-ENCA', config_file=None, https=True)

    def find_first_item(self, category_id, item):
        logger.info("Returning the first listing in the list")
        return self.get_all_listings(category_id, item)[0]

    def find_all_items(self, category_id, item):
        logger.info("Returning all listings. listing")
        listings = self.get_all_listings(category_id, item)
        logger.info(f"Found {len(listings)} items")

        return listings

    def get_all_listings(self, category_id, item) -> List:
        """
        Search Ebay for cards provided
        """
        try:
            logger.info("Check to see if item exists in cache")
            cached_listing = self.cache_get_listing(item)

            if cached_listing is None:
                logger.info("Listing not found in cache, querying eBay instead")
                response = self.query_ebay(item, category_id)
                listing = self.parse_results(response.dict(), item)
            else:
                logger.info(f"Returning cached listing for card={item}")
                listing = cached_listing

            return listing

        except ConnectionError as e:
            logger.error(e)

    def query_ebay(self, item, category_id):
        """
        Send request to eBay
        """
        params = self.build_search(item, category_id)
        logger.info("Sending API request to eBay")

        response = self.api.execute('findItemsAdvanced', params)

        return response

    def build_search(self, card, category_id) -> Dict:
        """
        Returns the search parameters
        """

        params = dict()
        params['keywords'] = card
        params['categoryId'] = category_id
        params['paginationInput'] = [
            {
             'entriesPerPage': 10
             }
        ]
        params['sortOrder'] = 'PricePlusShippingLowest'
        params['itemFilter'] = self.search_filters()

        return params

    # noinspection PyMethodMayBeStatic
    def search_filters(self) -> List:
        """
        Returns an array of filters for the search
        """

        return [
            {
                'name': 'ListingType',
                'value': ['FixedPrice', 'StoreInventory'],
            },
            {
                'name': 'PaymentMethod',
                'value': 'PayPal',
            },
            {
                'name': 'HideDuplicateItems',
                'value': True,
            },
            {
                'name': 'Currency',
                'value': 'CAD',
            }
        ]

    def parse_results(self, result, card_key):
        """
        Checks if any items were received, if not return default values
        """

        if self.has_listing(result):
            logger.info("Found listing for {}. Parsing data.".format(card_key))
            listings = self.populate_data_from_listing(result, card_key)
            self.cache_found_listings(card_key, listings)

        else:
            logger.warn("No listing found for card \"{}\"".format(card_key))
            logger.warn("Returning default values")
            listings = self.get_empty_listing(card_key)

        return listings

    # noinspection PyMethodMayBeStatic
    def has_listing(self, result):
        """
        Checks if there are any results returned by eBay AP
        """
        has_listing = False
        count = result['searchResult']['_count']

        if int(count) > 0:
            has_listing = True

        return has_listing

    # noinspection PyMethodMayBeStatic
    def get_empty_listing(self, card_name):
        """
        Returns a list of a default Card item
        """
        return [Card(attributes=None, card_key=card_name, default_name=card_name)]

    # noinspection PyMethodMayBeStatic
    def populate_data_from_listing(self, result, card_key):
        """
        Return dict with relevant listing info
        """
        logger.debug("Dumping items from result")
        logger.debug(result)

        items = result['searchResult']['item']
        list_of_cards = list()

        for item in items:
            list_of_cards.append(Card(attributes=item, card_key=card_key))

        return list_of_cards

    # noinspection PyMethodMayBeStatic
    def cache_found_listings(self, card, listings):
        """
        Stores the listings in cache.
        Use DB card name as the key
        """
        result = cache.set(card, listings)

        if not result:
            logger.error("Was not able to add item to cache!")
        else:
            logger.info(f"Listing was add to cache with key={card}")

    # noinspection PyMethodMayBeStatic
    def cache_get_listing(self, card_key):
        """
        Get listing in cache
        """

        return cache.get(card_key)


class Card:
    def __init__(self, attributes, card_key, default_name=""):
        self.title = default_name
        self.url = '-'
        self.image = 'http://getdrawings.com/free-icon/small-question-mark-icon-55.jpg'
        self.price = '-'
        self.shipping = '-'
        self.listing_type = 'No item found'
        self.found_listing = False
        self.key = card_key

        if attributes is not None:
            self.unwrap_attributes(attributes)

    def __repr__(self):
        return repr([self.title, self.url, self.key])

    def unwrap_attributes(self, attributes):
        self.title = attributes['title']
        self.url = attributes['viewItemURL']
        self.image = attributes['galleryURL']
        self.price = attributes['sellingStatus']['currentPrice']['value']
        self.shipping = attributes['shippingInfo']['shippingServiceCost']['value']
        self.listing_type = attributes['listingInfo']['listingType']
        self.found_listing = True
